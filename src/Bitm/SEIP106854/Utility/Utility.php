<?php

 namespace Rasel\BITM\SEIP106854\Utility;
 include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
class Utility{
    
    public $message="";
    static public function d($param=false){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    
    static public function dd($param=false){
        self::d($param);
        die();
    }
    
    static public function redirect($url="/AtomicProject/Views/SEIP106854/TvModel/index.php"){
        header("Location:".$url);
    }
    static public function redirect1($url="/AtomicProject/Views/SEIP106854/Date/index.php"){
        header("Location:".$url);
    }
    static public function redirect2($url="/AtomicProject/Views/SEIP106854/TextSummary/index.php"){
        header("Location:".$url);
    }
     static public function redirect3($url="/AtomicProject/Views/SEIP106854/Gender/index.php"){
        header("Location:".$url);
    }
       static public function redirect4($url="/AtomicProject/Views/SEIP106854/File/index.php"){
        header("Location:".$url);
    }
      static public function redirect5($url="/AtomicProject/Views/SEIP106854/City/index.php"){
        header("Location:".$url);
    }
        static public function redirect6($url="/AtomicProject/Views/SEIP106854/Subscribe/index.php"){
        header("Location:".$url);
    }


    static public function message($_message=null){
        if(is_null('message')){ // please give me message
          $_message=$_SESSION['message'];
            $_SESSION['message']=null;
            return $_message;
        }else{ //please set this message
            $_SESSION['message']=$_message;
          
            
        }
    }
    
    


}
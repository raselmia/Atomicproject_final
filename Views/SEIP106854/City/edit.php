
<?php

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \Rasel\BITM\SEIP106854\City\City;
use  \Rasel\BITM\SEIP106854\Utility\Utility;

$city = new City();
$city= $city->show($_GET['id']);

//Utility::dd($book);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../style.css">
        <!-- Bootstrap -->
        <link href="../../../Bootstrap/css/bootstrap.min.css" rel="stylesheet">

    </head>
     <?php 



       include_once "../../../page/header.php";




    ?>
    <body>
        <div class="container">
        <form action="update.php" method="post">
            <fieldset>
                <legend>Edit City  </legend>
                     <input  
                           type="hidden" 
                           name="id"
                           value="<?php echo $city['id'];?>"
                           />
                <div>
                    <label>Enter Book Title</label>
                    <input autofocus="autofocus" 
                    
                           placeholder="Enter the title of your favorite book" 
                           type="text" 
                           name="name"
                     
                           required="required"
                           value="<?php echo $city['name'];?>"
                           />
                 </div>
             <label>Select your City</label>
                        <select name="city"class="form-control">
                         <option>Select</option>   
                     <option>Dhaka</option>
                           <option>Barishal</option>
                        <option>Khulna</option>
                    <option>Munshigonj</option>
                         <option>Chittagong</option>
                        </select><br>
                     
                        <button class="btn btn-success" type="submit">Save</button>
                        <button class="btn btn-primary" type="submit">Save & Add Again</button>
<!--                <input type="submit" value="Save" />-->
                        <input class="btn btn-info"type="reset" value="Reset" />
            </fieldset>
        </form> 
        <nav>
            <li class="btn btn-primary"><a href="index.php">Go to List</a></li>
            <li class="btn btn-success right"><a href="javascript:history.go(-1)">Back</a></li>
        </nav>
        </div>
    </body>
     <section>
            <?php
include_once "../../../page/footer.php";


              ?>
        </section>
</html>





<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');
    
    use \Rasel\BITM\SEIP106854\Book\Book;
    use  \Rasel\BITM\SEIP106854\Utility\Utility;
    
    $book = new Book();
    $books = $book->index();
    
    
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Book-List</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
            #utility{
                float:right;
               
            }
            #message{
                background-color:green;
                color: white;
            }

        </style>
    </head>
    <section>
            <?php
            include_once "../../../page/header.php";
            ?>
        </section>
    
    <body>
        <div class='container'>
        <h1>Book Title</h1>
        
        <div id="message">
            <?php echo Utility::message(); ?>            
        </div>
        
        <div><span>Search / Filter </span> 
            <span id="utility">Download as PDF | XL  <a class="btn btn-success"
                                                        href="create.php">Add New</a></span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Sl.</th>
              
                    <th>Book Title &dArr;</th>
                     <th>Author &dArr;</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               <?php
               $slno =1;
               foreach($books as $book){
               ?>
                <tr>
                    <td><?php echo $slno;?></td>
                   
                    <td><a href="show.php?id=<?php echo $book['id'];?>"><?php echo $book['title'];?></a></td>
                    <td><?php echo $book['author'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $book['id'];?>">View</a>
                        | <a href="edit.php?id=<?php echo $book['id'];?>">Edit</a> 
                        
                        | <a href="delete.php?id=<?php echo $book['id'];?>" class="delete">Delete</a>
                        <form action="delete.php" method="post">
                            <input type="hidden" name ="id" value="<?php echo $book['id'];?>">
                      
                        </form>
                        | Trash/Recover | Email to Friend </td>
                </tr>
            <?php
           $slno++;
            }
            ?>
            </tbody>
        </table>
        
        <ul class="pagination">
  <li><a href="#">1</a></li>
  <li class="active"><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
</ul>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
        <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 
    
    
    $('#message').hide(10);
        </script>
        </div>
         </body>
        <section>
            <?php
            include_once "../../../page/footer.php";
            ?>
        </section>
   
</html>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Atomic project</title>

        <!-- Bootstrap -->
        <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="style.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <section>
            <?php
            include_once "page/header.php";
            ?>
        </section>

        <div class="container">
            <div class="jumbotron">
                <h1>BITM - Web App Dev - PHP</h1>
                <table>
                    <thead>
                    <th>Group Name</th>
                    <th>:</th>
                    <th>4-Axis</th>



                    </thead>

                    <tbody>
                        <tr>

                            <td>ID</td>
                            <td>:</td>
                            <td>SEIP106854</td>

                        </tr>
                        <tr>
                            <td>BATCH</td>
                            <td>:</td>
                            <td>11</td>



                        </tr>
                    </tbody>




                </table>


            </div>
            <h2>Projects</h2>

            <div>
                <table class="table table-hover alert-success">
                    <thead>
                        <tr>

                    <td>SL.No</td>
                    <td>Project Name</td>


                        </tr>


                    </thead>


                    <tbody>
                    <tr>
                            <td>01</td>
                            <td> <a href="Views\SEIP106854\Book">Book</a></td>




                        </tr>


                       


                        <tr>
                            <td>02</td>
                            <td><a href="Views\SEIP106854\Date">DATE</a></td>




                        </tr>

                        <tr>
                            <td>03</td>
                            <td><a href="Views\SEIP106854\Textsummary">TEXTSUMMARY</a></td>




                        </tr>

                        <tr>
                            <td>04</td>
                            <td><a href="Views\SEIP106854\File">FILE</a></td>




                        </tr>
                        <tr>
                            <td>05</td>
                            <td> <a href="Views\SEIP106854\Hobby">Hobby</a></td>




                        </tr>
                        <tr>
                            <td>06</td>
                            <td> <a href="Views\SEIP106854\City">City</a></td>




                        </tr>
                        <tr>
                            <td>07</td>
                            <td> <a href="Views\SEIP106854\Subscribe">Email</a></td>




                        </tr>
                        <tr>
                            <td>08</td>
                            <td><a href="Views\SEIP106854\Terms_condition">Terms&condition</a></td>




                        </tr>
                        
                        <tr>
                            <td>09</td>
                            <td><a href="Views\SEIP106854\Gender">Gender</a></td>




                        </tr>
                    </tbody>


                </table>



            </div>


        </div>
        <section>
            <?php
            include_once "page/footer.php";
            ?>
        </section>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="Bootstarp/js/bootstrap.min.js"></script>
    </body>
</html>
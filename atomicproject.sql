-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2016 at 05:43 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdays`
--

CREATE TABLE IF NOT EXISTS `birthdays` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthdays`
--

INSERT INTO `birthdays` (`id`, `name`, `date`) VALUES
(3, 'Rasel Mia', '2016-01-15'),
(4, 'tuhin', '2016-01-23'),
(5, 'Rasel Mia', '2015-12-27'),
(6, 'Ashik', '2016-01-21'),
(15, 'Rasel Mia', '2016-01-07'),
(17, 'Arif', '2016-01-05'),
(24, 'rasel Mia', '2016-01-08'),
(25, 'Rasel Mia', '2016-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`) VALUES
(41, 'JavaScript', 'Rasel Mia'),
(42, 'php', 'Rasel Mia'),
(43, 'php', 'Rasel Mia'),
(44, 'java', 'Rasel Mia'),
(45, 'Php', 'Ashik'),
(46, 'reatre', 'erytr');

-- --------------------------------------------------------

--
-- Table structure for table `citys`
--

CREATE TABLE IF NOT EXISTS `citys` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `citys`
--

INSERT INTO `citys` (`id`, `name`, `city`) VALUES
(6, 'Rasel Mia', 'Khulna'),
(8, 'Rasel Mia', 'Khulna'),
(9, 'Rasel Mia', 'Dhaka'),
(10, 'Rasel Mia', 'Khulna');

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE IF NOT EXISTS `genders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `name`, `gender`) VALUES
(5, 'Rahima', 'Female'),
(6, 'Kamal', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `hobbys`
--

CREATE TABLE IF NOT EXISTS `hobbys` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `hoby1` text NOT NULL,
  `hoby2` text NOT NULL,
  `hoby3` text NOT NULL,
  `hoby4` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbys`
--

INSERT INTO `hobbys` (`id`, `name`, `hoby1`, `hoby2`, `hoby3`, `hoby4`) VALUES
(8, 'Rasel Mia', 'Cricket', '', '', ''),
(9, 'rasel Mia', 'Cricket', 'Song', 'Movie', ''),
(10, 'hdh', '', 'Song', 'Movie', ''),
(11, 'fddf', 'Cricket', 'Song', '', ''),
(12, 'dhgh', 'Cricket', 'Song', '', ''),
(13, 'shg', 'Cricket', '', '', ''),
(14, 'af', 'Cricket', '', '', ''),
(16, 'ffAD', 'Cricket', 'Song', 'Movie', ''),
(17, 'rASEL mIA', 'Cricket', '', '', ''),
(18, 'ag', 'Cricket', 'Song', '', ''),
(19, 'asDF', 'Cricket', 'Song', '', ''),
(21, 'avfdbdf', '', '', '', ''),
(30, 'rasel Mia', '', '', '', ''),
(32, 'Rasel Mia', '', '', '', ''),
(33, 'rasel MIa', '', '', '', ''),
(35, 'Rasel Mia', '', '', '', ''),
(36, 'Rasel Mia', '', '', '', ''),
(37, 'Rasel Mia', 'Cricket', 'Song', 'Movie', ''),
(40, 'Rasel Mia', 'Cricket', 'Song', 'Movie', ''),
(43, 'Rasel Mia', 'Cricket', 'Song', 'Movie', ''),
(46, 'Imaran', 'Cricket', 'Song', 'Movie', 'Swimming'),
(49, 'rasel Mia', 'Cricket', 'Song', 'Movie', '');

-- --------------------------------------------------------

--
-- Table structure for table `profilepictures`
--

CREATE TABLE IF NOT EXISTS `profilepictures` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `photo` blob NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepictures`
--

INSERT INTO `profilepictures` (`id`, `name`, `photo`) VALUES
(1, '', 0x63616c66312e6a7067),
(2, '', 0x63616c66312e6a7067),
(3, '', 0x63616c66312e6a7067),
(4, '', 0x63616c66312e6a7067),
(5, '', 0x63616c66312e6a7067),
(6, '', 0x63616c66312e6a7067),
(7, '', 0x7069632e6a7067),
(8, '', ''),
(9, '', 0x4e49442d312e6a7067),
(10, '', 0x4e49442d322e6a7067),
(11, '', ''),
(12, '', 0x7069632e6a7067),
(13, '', 0x313230783135302e6a7067),
(14, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `name`, `email_address`) VALUES
(5, 'Rasel Mia', 'raselmia@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `summarys`
--

CREATE TABLE IF NOT EXISTS `summarys` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summarys`
--

INSERT INTO `summarys` (`id`, `title`, `summary`) VALUES
(30, 'Nur-Alam', 'I am a student of BITM\r\n'),
(31, 'Rasel Mia', 'My name is Rasel'),
(32, 'Rasel Mia', 'hellow'),
(33, 'Rasel Mia', 'write something'),
(34, '', ''),
(39, 'Ashik Rahman', 'ADFGGSD'),
(40, 'hghjgjhsdgc', 'bhjgcjhkajsd');

-- --------------------------------------------------------

--
-- Table structure for table `termsconditions`
--

CREATE TABLE IF NOT EXISTS `termsconditions` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `condition` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `termsconditions`
--

INSERT INTO `termsconditions` (`id`, `name`, `condition`) VALUES
(0, 'rasel Mia', 'Are yoy Agree the Above Condition');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthdays`
--
ALTER TABLE `birthdays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `citys`
--
ALTER TABLE `citys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbys`
--
ALTER TABLE `hobbys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepictures`
--
ALTER TABLE `profilepictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summarys`
--
ALTER TABLE `summarys`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthdays`
--
ALTER TABLE `birthdays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `citys`
--
ALTER TABLE `citys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hobbys`
--
ALTER TABLE `hobbys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `profilepictures`
--
ALTER TABLE `profilepictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `summarys`
--
ALTER TABLE `summarys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
